import { TimeSpanTypeEnum } from './api/timeSpanTypeEnum';

export class WorkTime {
  'date'?: string;
  'from'?: string;
  'to'?: string;
  'type': TimeSpanTypeEnum;
  'project'?: number;
  'homeoffice'?: boolean;
}

export class ProjectTime {
  'date'?: string;
  'from': string;
  'to': string;
  'project'?: number;
}