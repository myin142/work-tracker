import {
  areIntervalsOverlapping,
  format,
  Interval,
  intervalToDuration,
  isBefore,
  parse,
  parseISO,
  add,
  startOfToday,
} from 'date-fns';
import { groupBy } from 'lodash';
import { ProjectTime, WorkTime } from '.';
import { TimeSpanTypeEnum } from './api/timeSpanTypeEnum';

class ParsedTime {
  'interval': Interval;
  'project'?: number;
}
export type TotalHours = {
  [s: string]: string;
};

export class TimeBookingParser {

  getTotalHours(workTime: WorkTime[]): TotalHours {
    const grouped = groupBy(workTime, 'type');
    const result: TotalHours = {};

    for (const type of Object.keys(grouped)) {
      if (type === TimeSpanTypeEnum.Work || type === TimeSpanTypeEnum.Break) {
        result[type] = this.countTotalHours(grouped[type]);
      }
    }

    return result;
  }

  private countTotalHours(workTime: WorkTime[]): string {
    const start = new Date();
    const end = workTime.map(t => ({ start: this.parseTime(t.from), end: this.parseTime(t.to) }))
      .map(i => intervalToDuration(i))
      .reduce((prev, curr) => add(prev, curr), start);

    const time = add(startOfToday(), intervalToDuration({ start, end }));
    return format(time, 'HH:mm');
  }

  getProjectsForWorkTime(workTime: WorkTime, projectTimes: ProjectTime[]): number {
    const workInterval: Interval = { start: this.parseTime(workTime.from), end: this.parseTime(workTime.to) };

    const overlapping = projectTimes
      .filter(p => p.date === workTime.date)
      .filter(p => areIntervalsOverlapping(workInterval, { start: this.parseTime(p.from), end: this.parseTime(p.to) }));

    return overlapping.length > 0 ? overlapping[0].project || -1 : -1;
  }

  parseWorkTimes(input: string, project: number = -1): WorkTime[] {
    return input
      .split(',')
      .map((i) => this.parseWorkTime(i, project))
      .reduce((prev, curr) => prev.concat(curr), []);
  }

  private parseWorkTime(input: string, defaultProj: number): WorkTime[] {
    const times = input.split('/');
    const { interval: workTime, project: p } = this.parseInterval(times[0]);
    const project = (p != null) ? p : defaultProj;

    const result = [];

    if (times.length > 1) {
      const { interval: breakTime } = this.parseInterval(times[1]);
      result.push(this.intervalToTime(breakTime, TimeSpanTypeEnum.Break));

      if (areIntervalsOverlapping(workTime, breakTime)) {
        result.push(
          this.intervalToTime(
            { start: workTime.start, end: breakTime.start },
            TimeSpanTypeEnum.Work,
            project,
          ),
        );

        if (isBefore(breakTime.end, workTime.end)) {
          result.push(
            this.intervalToTime(
              { start: breakTime.end, end: workTime.end },
              TimeSpanTypeEnum.Work,
              project,
            ),
          );
        }
      }
    } else {
      result.push(this.intervalToTime(workTime, TimeSpanTypeEnum.Work, project));
    }

    return result.sort((a, b) => {
      if (!a.from) return -1;
      if (!b.from) return 1;
      return a.from.localeCompare(b.from);
    });
  }

  private intervalToTime({ start, end }: Interval, type: TimeSpanTypeEnum, project?: number): WorkTime {
    const time: WorkTime = {
      type,
    };

    const from = format(start, 'HH:mm');
    if (from !== '00:00') {
      time.from = from;
    }

    const to = format(end, 'HH:mm');
    if (to !== '00:00') {
      time.to = to;
    }

    if (time.from && time.to) {
      time.project = project;
    }

    return time;
  }

  private parseInterval(time: string): ParsedTime {
    let project;

    const projectSplit = time.split('#');
    if (projectSplit.length > 1) {
      const parsedProject = parseInt(projectSplit[0]);
      if (!isNaN(parsedProject)) {
        project = parsedProject;
      }
    }

    const timeWithRange = projectSplit.length > 1 ? projectSplit[1] : projectSplit[0];
    const ranges = timeWithRange.split('-');

    const start = this.parseTime(ranges[0]);
    const end = this.parseTime(ranges[1]);

    return {
      interval: { start, end },
      project,
    };
  }

  parseTime(time?: string, dateStr?: string): Date {
    if (!time) return startOfToday();

    const f = time.includes(':') ? 'HH:mm' : 'HH';
    const date = dateStr ? parseISO(dateStr) : new Date();
    return parse(time, f, date);
  }
}
