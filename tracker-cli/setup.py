from setuptools import setup
from os import path, getcwd

with open('requirements.txt', 'r', encoding='utf-8') as f:
    requirements = f.read()

path_to_my_project = path.join(
    getcwd(), '..', 'dist', 'python/myin.work_tracker-0.0.0-py3-none-any.whl')

setup(
    name='work_tracker',
    version='0.1.0',
    py_modules=['tracker_cli'],
    install_requires=[
        requirements,
        # f'myin.work_tracker @ file://localhost/{path_to_my_project}#egg=myin.work_tracker'
    ],
    packages=['tracker_cli', 'client.openapi_client'],
    entry_points='''
        [console_scripts]
        worktracker=tracker_cli:main
    '''
)
