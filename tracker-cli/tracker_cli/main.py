from itertools import groupby
import json
import os
from argparse import ArgumentParser
from datetime import date, datetime, timedelta
from json.decoder import JSONDecodeError

from openapi_client.exceptions import ApiException

from myin.work_tracker import TimeBookingParser

from .mapper import from_work_time_to_project_time
from .tracker_api import TrackerAPI
from .tracker_ui import TrackerUI

def main():
    parser = ArgumentParser()

    # List command (default)
    parser.add_argument('--week', default=False, action='store_true', help='list booking for week')

    # Save command
    parser.add_argument('--save', default=False, action='store_true', help='save some properties as default')
    parser.add_argument('--api-key', type=str, help='API key')
    parser.add_argument('--project', type=int, help='default project if none specified in input')

    # Add work time command
    parser.add_argument('--add', nargs='?', const='', help='add work time input')
    parser.add_argument('--home', '--homeoffice', default=False, action='store_true', help='flag as homeoffice')
    parser.add_argument('--overwrite', default=False, action='store_true', help='overwrite previous bookings by clearing everything')
    parser.add_argument('--vacation', default=False, action='store_true', help='set full vacation for date')
    parser.add_argument('--half-vacation', default=False, action='store_true', help='set half vacation for date')
    parser.add_argument('--sick', default=False, action='store_true', help='set sick day')

    # TODO: submit/withdraw times

    # List project command
    parser.add_argument('--list-projects', default=False, action='store_true', help='list projects')

    # Other
    parser.add_argument('--date', type=lambda s: datetime.strptime(s, '%Y-%m-%d').date(), default=date.today(), help='date for action')
    parser.add_argument('-v', '--verbose', default=False, action='store_true', help='log more outputs')

    args = parser.parse_args()

    SETTING_FILE = 'work-time-setting.json'

    setting = {}

    if os.path.isfile(SETTING_FILE):
        try:
            with open(SETTING_FILE, 'r') as f:
                loaded_setting = json.load(f)
                setting.update(loaded_setting)
        except (IOError, JSONDecodeError) as e:
            print('Failed to load setting file: ', e)

    if args.api_key:
        setting['api_key'] = args.api_key

    if args.project:
        setting['project'] = args.project

    if args.save:
        if not os.path.exists(SETTING_FILE) or os.path.isfile(SETTING_FILE):
            with open(SETTING_FILE, 'w') as f:
                if args.verbose:
                    print('Saved settings as default')
                json.dump(setting, f)

    if args.verbose:
        print('Using settings', setting)

    if not 'api_key' in setting:
        print('Missing api key')
        exit()

    if not 'project' in setting:
        print('Missing project id')
        exit()

    ui = TrackerUI()
    tracker = TrackerAPI(setting['api_key'])
    date_iso: date = args.date
    parser = TimeBookingParser()

    if args.verbose:
        print(f'Running for date {date_iso}')

    try:
        if not args.add is None:
            if args.overwrite:
                if args.verbose:
                    print(f'Deleting previous time bookings')
                tracker.delete_project_time(date=date_iso)
                tracker.delete_time(date=date_iso)

            if args.verbose:
                print(f'Adding time for {date_iso.isoformat()} with homeoffice {args.home}')
            times = parser.parse_work_times(args.add, setting['project'])

            type = None
            if args.sick:
                type = 'SICK_LEAVE'
            elif args.vacation:
                type = 'FULL_DAY_VACATION'
            elif args.half_vacation:
                type = 'HALF_DAY_VACATION'

            if type:
                for time in times:
                    time.type = type

            homeoffice = args.home if args.home else False
            for time in times:
                time.homeoffice = homeoffice
                time.date = date_iso.isoformat()
                if args.verbose:
                    ui.print_time(time)
                tracker.add_time(date=date_iso, work_time=time)

            for project, values in groupby(times, key=lambda x: x.project):
                if project:
                    project_times = [from_work_time_to_project_time(i) for i in values]
                    if args.verbose:
                        ui.print_project_times(project=project, project_times=project_times)
                    tracker.add_project_time(date=date_iso, project=project, time_spans=project_times)

            ui.print_times(times=times)
        elif args.list_projects:
            projects = tracker.get_projects()
            ui.print_projects(projects)
        else:
            firstdate = date_iso - timedelta(days=date_iso.weekday())
            lastdate = firstdate + timedelta(days=6)

            start = firstdate if args.week else date_iso
            end = lastdate if args.week else date_iso

            if args.verbose:
                print(f'Getting time booking {start} - {end}')

            times = tracker.get_times(start=start, end=end)
            project_times = tracker.get_project_time(start=start, end=end)

            for time in times:
                time.project = parser.get_projects_for_work_time(time, project_times)

            ui.print_times(times=times)
    except ApiException as e:
        ui.print_error(e)