from myin.work_tracker import ProjectTime, WorkTime
from openapi_client.model.time_span_with_id import TimeSpanWithID
from openapi_client.model.project_date_time_spans import ProjectDateTimeSpans
from openapi_client.model.project_time_span import ProjectTimeSpan
from openapi_client.model.time import Time

def to_work_time(timeSpan: TimeSpanWithID) -> WorkTime:
    time = timeSpan._data_store
    work_time = WorkTime()
    if 'to' in time:
        work_time.to = time['to']['value']
    if '_from' in time:
        work_time.from_ = time['_from']['value']
    work_time.type = time['type']['value']
    work_time.date = time['date'].isoformat()
    work_time.homeoffice = time['homeoffice']
    return work_time

def to_project_times(projectTime: ProjectDateTimeSpans) -> list[ProjectTime]:
    result = []

    for time in projectTime['time_spans']:
        project_time = ProjectTime()
        project_time.from_ = time['_from']['value']
        project_time.to = time['to']['value']
        project_time.project = projectTime['project']
        result.append(project_time)
    
    return result

def from_project_time(project_time: ProjectTime) -> ProjectTimeSpan:
    return ProjectTimeSpan(to=Time(project_time.to), _from=Time(project_time.from_))

def from_work_time_to_project_time(work_time: WorkTime) -> ProjectTime:
    time = ProjectTime()
    time.date = work_time.date
    time.from_ = work_time.from_
    time.to = work_time.to
    time.project = work_time.project if work_time.project else -1
    return time