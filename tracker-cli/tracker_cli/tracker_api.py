from myin.work_tracker import ProjectTime, WorkTime
from openapi_client.api_client import ApiClient, Configuration
from openapi_client.api.default_api import DefaultApi
from openapi_client.model.project_date_time_spans import ProjectDateTimeSpans
from openapi_client.model.project_name_id_map import ProjectNameIDMap
from openapi_client.model.time import Time
from openapi_client.model.time_span_type_enum import TimeSpanTypeEnum
from openapi_client.model.time_span_without_id import TimeSpanWithoutID
from .mapper import from_project_time, to_project_times, to_work_time


class TrackerAPI():
    def __init__(self, key: str, env: str = 'DEV') -> None:
        url = "https://ims-dev.it-experts.at" if env == "DEV" else "https://ims.it-experts.at"
        config = Configuration(host=f'{url}/api/v1', api_key={'ApiKey': key})
        api = ApiClient(configuration=config)
        self.client = DefaultApi(api_client=api)

    def get_projects(self) -> list[ProjectNameIDMap]:
        return self.client.project_get().projects

    def add_time(self, date: str, work_time: WorkTime):
        homeoffice = work_time.homeoffice if work_time.homeoffice else False
        time = TimeSpanWithoutID(
            date=date, homeoffice=homeoffice, type=TimeSpanTypeEnum(work_time.type))

        if work_time.to:
            time['to'] = Time(work_time.to)

        if work_time.from_:
            time['_from'] = Time(work_time.from_)

        return self.client.time_booking_post(time_span_without_id=time)

    def add_project_time(self, date: str, project: int, time_spans: list[ProjectTime]):
        project_time_spans = [from_project_time(i) for i in time_spans]
        project_times = ProjectDateTimeSpans(
            date=date, project=project, time_spans=project_time_spans)
        return to_project_times(self.client.project_time_booking_post(project_date_time_spans=project_times))

    def get_times(self, start: str, end: str) -> list[WorkTime]:
        times = self.client.time_booking_get(_from=start, to=end).time_spans
        return [to_work_time(i) for i in times]

    def get_project_time(self, start: str, end: str) -> list[ProjectTime]:
        projectTimeSpans = self.client.project_time_booking_get(
            _from=start, to=end).project_time_spans
        result = []

        for p in projectTimeSpans:
            result += to_project_times(p)

        return result

    def delete_time(self, date: str):
        for time in self.client.time_booking_get(to=date, _from=date).time_spans:
            self.client.time_booking_time_span_id_delete(
                time_span_id=time['id'])

    def delete_project_time(self, date: str):
        self.client.project_time_booking_delete(day=date)
