from myin.work_tracker import ProjectTime, TimeBookingParser, WorkTime
from itertools import groupby
from openapi_client.exceptions import ApiException
from openapi_client.model.project_name_id_map import ProjectNameIDMap

class Colors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    ITALIC = '\033[3m'

def color(str, *colors):
    result = str
    for c in colors:
        result = f'{c}{result}{Colors.ENDC}'
    return result

parser = TimeBookingParser()

class TrackerUI():

    def print_times(self, times: list[WorkTime]):
        times.sort(key=lambda x: x.date)
        for key, values in groupby(times, key=lambda x: x.date):
            time = []
            for t in values:
                time.append(t)

            homeoffice = len([i for i in time if i.homeoffice]) > 0
            if key:
                print(color(f'Date: {key} {"in homeoffice" if homeoffice else ""}', Colors.HEADER, Colors.BOLD))

            for t in time:
                self.print_time(time=t)

            self.print_total_hours(time)

    def print_time(self, time: WorkTime):
        print(f'{time.from_} - {time.to} | {time.type}, {time.project}')
    
    def print_total_hours(self, times: list[WorkTime]):
        hours = parser.get_total_hours(times)
        result = ''
        for key, value in hours.items():
            if len(result) > 0:
                result += ', '
            result += f'{key}: {value}'
        print(color(result, Colors.OKGREEN, Colors.ITALIC))

    def print_project_times(self, project: int, project_times: list[ProjectTime]):
        print(f'Project: {project}')
        for p in project_times:
            self.print_project_time(p)

    def print_project_time(self, project_time: ProjectTime):
        print(f'{project_time.from_}-{project_time.to}')

    def print_projects(self, projects):
        for project in projects:
            self.print_project(project)

    def print_project(self, project: ProjectNameIDMap):
        print(f'{project["project_id"]}: {project["project_name"]}')

    def print_error(self, ex: ApiException):
        print(color(f'Error: {ex.body}', Colors.FAIL))
