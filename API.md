# API Reference <a name="API Reference"></a>



## Classes <a name="Classes"></a>

### ProjectTime <a name="work-tracker.ProjectTime"></a>

#### Initializers <a name="work-tracker.ProjectTime.Initializer"></a>

```typescript
import { ProjectTime } from 'work-tracker'

new ProjectTime()
```



#### Properties <a name="Properties"></a>

##### `from`<sup>Required</sup> <a name="work-tracker.ProjectTime.property.from"></a>

```typescript
public readonly from: string;
```

- *Type:* `string`

---

##### `to`<sup>Required</sup> <a name="work-tracker.ProjectTime.property.to"></a>

```typescript
public readonly to: string;
```

- *Type:* `string`

---

##### `date`<sup>Optional</sup> <a name="work-tracker.ProjectTime.property.date"></a>

```typescript
public readonly date: string;
```

- *Type:* `string`

---

##### `project`<sup>Optional</sup> <a name="work-tracker.ProjectTime.property.project"></a>

```typescript
public readonly project: number;
```

- *Type:* `number`

---


### TimeBookingParser <a name="work-tracker.TimeBookingParser"></a>

#### Initializers <a name="work-tracker.TimeBookingParser.Initializer"></a>

```typescript
import { TimeBookingParser } from 'work-tracker'

new TimeBookingParser()
```

#### Methods <a name="Methods"></a>

##### `getProjectsForWorkTime` <a name="work-tracker.TimeBookingParser.getProjectsForWorkTime"></a>

```typescript
public getProjectsForWorkTime(workTime: WorkTime, projectTimes: ProjectTime[])
```

###### `workTime`<sup>Required</sup> <a name="work-tracker.TimeBookingParser.parameter.workTime"></a>

- *Type:* [`work-tracker.WorkTime`](#work-tracker.WorkTime)

---

###### `projectTimes`<sup>Required</sup> <a name="work-tracker.TimeBookingParser.parameter.projectTimes"></a>

- *Type:* [`work-tracker.ProjectTime`](#work-tracker.ProjectTime)[]

---

##### `getTotalHours` <a name="work-tracker.TimeBookingParser.getTotalHours"></a>

```typescript
public getTotalHours(workTime: WorkTime[])
```

###### `workTime`<sup>Required</sup> <a name="work-tracker.TimeBookingParser.parameter.workTime"></a>

- *Type:* [`work-tracker.WorkTime`](#work-tracker.WorkTime)[]

---

##### `parseTime` <a name="work-tracker.TimeBookingParser.parseTime"></a>

```typescript
public parseTime(time?: string, dateStr?: string)
```

###### `time`<sup>Optional</sup> <a name="work-tracker.TimeBookingParser.parameter.time"></a>

- *Type:* `string`

---

###### `dateStr`<sup>Optional</sup> <a name="work-tracker.TimeBookingParser.parameter.dateStr"></a>

- *Type:* `string`

---

##### `parseWorkTimes` <a name="work-tracker.TimeBookingParser.parseWorkTimes"></a>

```typescript
public parseWorkTimes(input: string, project?: number)
```

###### `input`<sup>Required</sup> <a name="work-tracker.TimeBookingParser.parameter.input"></a>

- *Type:* `string`

---

###### `project`<sup>Optional</sup> <a name="work-tracker.TimeBookingParser.parameter.project"></a>

- *Type:* `number`

---




### WorkTime <a name="work-tracker.WorkTime"></a>

#### Initializers <a name="work-tracker.WorkTime.Initializer"></a>

```typescript
import { WorkTime } from 'work-tracker'

new WorkTime()
```



#### Properties <a name="Properties"></a>

##### `type`<sup>Required</sup> <a name="work-tracker.WorkTime.property.type"></a>

```typescript
public readonly type: string;
```

- *Type:* `string`

---

##### `date`<sup>Optional</sup> <a name="work-tracker.WorkTime.property.date"></a>

```typescript
public readonly date: string;
```

- *Type:* `string`

---

##### `from`<sup>Optional</sup> <a name="work-tracker.WorkTime.property.from"></a>

```typescript
public readonly from: string;
```

- *Type:* `string`

---

##### `homeoffice`<sup>Optional</sup> <a name="work-tracker.WorkTime.property.homeoffice"></a>

```typescript
public readonly homeoffice: boolean;
```

- *Type:* `boolean`

---

##### `project`<sup>Optional</sup> <a name="work-tracker.WorkTime.property.project"></a>

```typescript
public readonly project: number;
```

- *Type:* `number`

---

##### `to`<sup>Optional</sup> <a name="work-tracker.WorkTime.property.to"></a>

```typescript
public readonly to: string;
```

- *Type:* `string`

---



