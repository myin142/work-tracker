const { JsiiProject } = require('projen');
const { DependencyType } = require('projen/lib/deps');

const apiPath = 'src/api';
const requiredApiFiles = ['timeSpanTypeEnum.ts'];

// More options here: https://github.com/projen/projen/blob/main/API.md#projen-jsiiproject
const core = new JsiiProject({
  author: 'myin',
  authorAddress: 'minyin142p@gmail.com',
  name: 'work-tracker',
  repositoryUrl: 'https://github.com/myin142/work-tracker.git',

  publishToPypi: {
    distName: 'myin.work-tracker',
    module: 'myin.work_tracker',
  },

  stale: false,
  depsUpgrade: false,
  buildWorkflow: false,
  release: true,
  defaultReleaseBranch: 'SOMETHING_RANDOM_SO_IT_DOES_NOT_GET_CALLED_JUST_WANT_THE_PROJEN_TASK',

  scripts: {
    'generate:api': `openapi-generator-cli generate -g typescript-angular -i openapi.yml -o ${apiPath} --global-property models && mv ${requiredApiFiles.map(f => apiPath + '/model/' + f).join('')} ${apiPath} && rm ${apiPath}/model -rf`,
    'generate:api-python': 'openapi-generator-cli generate -g python -i openapi.yml -o tracker-cli/client',
    'build:tracker-cli': 'cd tracker-cli && python setup.py bdist_wheel',
  },

});

core.buildTask.prependExec('npm run generate:api && npm run generate:api-python');
core.packageTask.exec('npm run build:tracker-cli');
core.packageTask.exec('mv tracker-cli/dist/* dist/');

core.deps.addDependency('@openapitools/openapi-generator-cli', DependencyType.DEVENV);
core.deps.addDependency('@types/lodash', DependencyType.DEVENV);

core.deps.addDependency('jest-mock-extended', DependencyType.TEST);

core.deps.addDependency('lodash', DependencyType.BUNDLED);
core.deps.addDependency('date-fns', DependencyType.BUNDLED);

core.eslint.addIgnorePattern('src/api/**');
core.addGitIgnore('src/api');
core.addGitIgnore('work-time-setting.json');
core.addGitIgnore('__pycache__');
core.addGitIgnore('tracker-cli/client');
core.addGitIgnore('tracker-cli/build');
core.addGitIgnore('tracker-cli/dist');
core.addGitIgnore('tracker-cli/work_tracker.egg-info');

core.eslint.addRules({
  'no-unused-vars': 'off',
});

core.synth();
