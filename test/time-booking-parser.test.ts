import { ProjectTime, WorkTime } from '../src';
import { TimeSpanTypeEnum } from '../src/api/timeSpanTypeEnum';
import { TimeBookingParser, TotalHours } from '../src/time-booking-parser';

let parser: TimeBookingParser;

beforeEach(() => {
  parser = new TimeBookingParser();
});

test('should parse time', () => {
  expect(parser.parseTime('10:00', '2020-01-01')).toEqual(
    new Date('2020-01-01T10:00:00'),
  );
});

describe('Parse Work Time', () => {
  test('should parse work time', () => {
    expect(parser.parseWorkTimes('08:00-17:00')).toEqual([
      expect.objectContaining({
        from: '08:00',
        to: '17:00',
        type: TimeSpanTypeEnum.Work,
      }),
    ]);
  });

  test('should parse work time hour shorthand', () => {
    expect(parser.parseWorkTimes('8-17')).toEqual([
      expect.objectContaining({
        from: '08:00',
        to: '17:00',
        type: TimeSpanTypeEnum.Work,
      }),
    ]);
  });

  test('should parse work time with project', () => {
    expect(parser.parseWorkTimes('8-17', 10)).toEqual([
      expect.objectContaining({
        project: 10,
      }),
    ]);
  });

  test('should not parse project for break time', () => {
    expect(parser.parseWorkTimes('8-17/12-13', 10)).toEqual(expect.arrayContaining([
      expect.objectContaining({
        type: TimeSpanTypeEnum.Break,
        project: undefined,
      }),
    ]));
  });

  test('should parse empty input as single time', () => {
    expect(parser.parseWorkTimes('', 10)).toEqual([{ type: TimeSpanTypeEnum.Work }]);
  });

  test('should parse only from time', () => {
    expect(parser.parseWorkTimes('08:00', 10)).toEqual([{ type: TimeSpanTypeEnum.Work, from: '08:00' }]);
  });

  test('should parse work time hour shorthand with breaks', () => {
    expect(parser.parseWorkTimes('8-17/12-13')).toEqual([
      expect.objectContaining({
        from: '08:00',
        to: '12:00',
        type: TimeSpanTypeEnum.Work,
      }),
      expect.objectContaining({
        from: '12:00',
        to: '13:00',
        type: TimeSpanTypeEnum.Break,
      }),
      expect.objectContaining({
        from: '13:00',
        to: '17:00',
        type: TimeSpanTypeEnum.Work,
      }),
    ]);
  });

  test('should parse work time hour shorthand with minutes', () => {
    expect(parser.parseWorkTimes('8:30-17')).toEqual([
      expect.objectContaining({
        from: '08:30',
        to: '17:00',
        type: TimeSpanTypeEnum.Work,
      }),
    ]);
  });

  test('should parse work time with break', () => {
    expect(parser.parseWorkTimes('08:00-17:00/12:00-13:00')).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          from: '08:00',
          to: '12:00',
          type: TimeSpanTypeEnum.Work,
        }),
        expect.objectContaining({
          from: '12:00',
          to: '13:00',
          type: TimeSpanTypeEnum.Break,
        }),
        expect.objectContaining({
          from: '13:00',
          to: '17:00',
          type: TimeSpanTypeEnum.Work,
        }),
      ]),
    );
  });

  test('should parse multiple work times', () => {
    expect(
      parser.parseWorkTimes('08:00-15:00/12:00-13:00,15:00-17:00/15:30-16:00'),
    ).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          from: '08:00',
          to: '12:00',
          type: TimeSpanTypeEnum.Work,
        }),
        expect.objectContaining({
          from: '12:00',
          to: '13:00',
          type: TimeSpanTypeEnum.Break,
        }),
        expect.objectContaining({
          from: '13:00',
          to: '15:00',
          type: TimeSpanTypeEnum.Work,
        }),

        expect.objectContaining({
          from: '15:00',
          to: '15:30',
          type: TimeSpanTypeEnum.Work,
        }),
        expect.objectContaining({
          from: '15:30',
          to: '16:00',
          type: TimeSpanTypeEnum.Break,
        }),
        expect.objectContaining({
          from: '16:00',
          to: '17:00',
          type: TimeSpanTypeEnum.Work,
        }),
      ]),
    );
  });

  test('should parse project', () => {
    expect(parser.parseWorkTimes('1#8-15')).toEqual([
      expect.objectContaining({
        project: 1,
      }),
    ]);
  });
});

describe('Get Projects For Work Time', () => {
  it('should get first overlapping project times', () => {
    const workTime: WorkTime = {
      from: '08:00',
      to: '10:00',
      type: TimeSpanTypeEnum.Work,
    };
    const projectTimes: ProjectTime[] = [
      { from: '08:00', to: '10:00', project: 1 },
      { from: '09:30', to: '11:00', project: 2 },
      { from: '10:00', to: '13:00', project: 3 },
    ];

    expect(parser.getProjectsForWorkTime(workTime, projectTimes)).toEqual(1);
  });

  it('should get overlapping project times of same date', () => {
    const workTime: WorkTime = {
      date: '2020-01-01',
      from: '08:00',
      to: '10:00',
      type: TimeSpanTypeEnum.Work,
    };
    const projectTimes: ProjectTime[] = [
      { from: '08:00', to: '10:00', project: 1 },
      { from: '09:30', to: '11:00', project: 2, date: '2020-01-01' },
      { from: '10:00', to: '13:00', project: 3 },
    ];

    expect(parser.getProjectsForWorkTime(workTime, projectTimes)).toEqual(2);
  });
});

describe('Get Total Hours', () => {
  it('count total hours for types', () => {
    const actual: TotalHours = parser.getTotalHours([
      { from: '08:00', to: '10:00', type: TimeSpanTypeEnum.Work },
      { from: '10:00', to: '11:00', type: TimeSpanTypeEnum.Break },
      { from: '12:00', to: '15:00', type: TimeSpanTypeEnum.Work },
    ]);

    expect(actual[TimeSpanTypeEnum.Work]).toEqual('05:00');
    expect(actual[TimeSpanTypeEnum.Break]).toEqual('01:00');
  });

  it('handle special types', () => {
    const actual: TotalHours = parser.getTotalHours([
      { from: '12:00', to: undefined, type: TimeSpanTypeEnum.SickLeave },
      {
        from: undefined,
        to: undefined,
        type: TimeSpanTypeEnum.HalfDayVacation,
      },
      {
        from: undefined,
        to: undefined,
        type: TimeSpanTypeEnum.FullDayVacation,
      },
    ]);

    expect(actual[TimeSpanTypeEnum.SickLeave]).toBeUndefined();
    expect(actual[TimeSpanTypeEnum.HalfDayVacation]).toBeUndefined();
    expect(actual[TimeSpanTypeEnum.FullDayVacation]).toBeUndefined();
  });
});
